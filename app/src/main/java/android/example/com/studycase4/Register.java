package android.example.com.studycase4;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends AppCompatActivity{
    TextView masuk;
    EditText nama, email, password;
    Button button;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    masuk = findViewById(R.id.txtMasuk);
    nama = findViewById(R.id.nama);
    email = findViewById(R.id.email_regis);
    password = findViewById(R.id.pswd_regis);
    button = findViewById(R.id.btn_regis);

        button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(Register.this, MainActivity.class));
        }
    });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (nama.getText().toString().isEmpty()) {
                    Toast.makeText(Register.this, "Nama harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (email.getText().toString().isEmpty()) {
                    Toast.makeText(Register.this, "Email harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.getText().toString().isEmpty()) {
                    Toast.makeText(Register.this, "Password harus diisi", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.getText().toString().length() < 6) {
                    Toast.makeText(Register.this, "Password minimal 6 karakter", Toast.LENGTH_SHORT).show();
                    return;

                }
            }
        });
    }

        }

